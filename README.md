# rgsrv

A rust gemini server from a noob programmer

## Just for learning purposes

This is a private little learning project on rust. It's based on the web server
developed in the [Rust Book](https://doc.rust-lang.org/book/ch20-00-final-project-a-web-server.html).

Although it is in early alpha state, I hope to release this as a stable Gemini
server some day.

## Supported features so far

* Static files
* CGI
* Client certificates
* virtual hosts
* Multithreaded worker pool

## Still TODO

* Serving from user directories
* Clean shutdown (signals)
* Reasonable logging
* Indexes
* Redirects
* Cleaning up the code a lot
* A lot more

## CGI Environment Variables

| Variable          | Explanation                                 |
| ---               | ---                                         |
| GATEWAY_INTERFACE | Always 'CGI/1.1'                            |
| GEMINI_URL        | The complete URL of the request             |
| SERVER_NAME       | The Server Name used in the request         |
| SERVER_PROTOCOL   | Always 'GEMINI'                             |
| REMOTE_ADDR       | IP address of the client                    |
| REMOTE_HOST       | Same as REMOTE_ADDR                         |
| REMOTE_PORT       | Client TCP port                             |
| SERVER_SOFTWARE   | 'rgsrv'                                     |
| QUERY_STRING      | Query part of the URL (after '?')           |
| SCRIPT_NAME       | Full script name                            |
| PATH_INFO         | Path information from URL                   |
| AUTH_TYPE         | 'Certificate' if client certificate is sent |
| REMOTE_USER       | Common Name of client certificate           |
| TLS_CLIENT_HASH   | SHA256 Hash of client certificate in hex    |

## Copied shamelessly from gemserv

All the good parts of the code were looked up from gemserv by int80h, although
he uses an asynchronous approach while I use threads (like in the book).

https://sr.ht/~int80h/gemserv/ gemserv git repo

## Don't use it in production (yet) - And always AT YOUR OWN RISK!

It's buggy, it's insecure, it panics, it breaks! Not to mention the whole
inadequate design on my end.

