#!/bin/bash
printf "20 text/gemini;lang=en\r\n"

printf "# CGI Variables\n"
[ -z ${AUTH_TYPE} ] && printf "Connect with client certificate to see its results\n\n"
printf '```'
printf "\n"
printf "GATEWAY_INTERFACE = %s\n" "${GATEWAY_INTERFACE}"
printf "GEMINI_URL = %s\n" "${GEMINI_URL}"
printf "SERVER_NAME = %s\n" "${SERVER_NAME}"
printf "SERVER_PROTOCOL = %s\n" "${SERVER_PROTOCOL}"
printf "\n"
printf "REMOTE_ADDR = %s\n" "${REMOTE_ADDR}"
printf "REMOTE_HOST = %s\n" "${REMOTE_HOST}"
printf "REMOTE_PORT = %s\n" "${REMOTE_PORT}"
printf "\n"
printf "SERVER_SOFTWARE = %s\n" "${SERVER_SOFTWARE}"
printf "\n"
printf "QUERY_STRING = %s\n" "${QUERY_STRING}"
printf "SCRIPT_NAME = %s\n" "${SCRIPT_NAME}"
printf "PATH_INFO = %s\n" "${PATH_INFO}"
printf '```'
printf "\n\n"

if [ "${AUTH_TYPE}" = "Certificate" ]; then
    printf "# Client Certificate Data\n"
    printf '```'
    printf "\n"
    printf "AUTH_TYPE = %s\n" "${AUTH_TYPE}"
    printf "REMOTE_USER = %s\n" "${REMOTE_USER}"
    printf "TLS_CLIENT_HASH = %s\n" "${TLS_CLIENT_HASH}"
    printf '```'
    printf "\n\n"
fi

