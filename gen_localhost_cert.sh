#!/bin/sh
#
# file: gen_localhost_cert.sh
#
# This shell script generates a private key and a certificate for localhost
# named "localhost.key" and "localhost.crt"
# 
# Just for testing purposes

openssl req -newkey rsa:4096 -days 3560 -nodes -x509 \
    -keyout localhost.key -out localhost.crt \
    -subj "/C=EU/ST=Gondor/L=Minas Tirith/O=SnakeOil Inc./OU=IT Department/CN=localhost"

