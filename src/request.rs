use std::io::{Error, Read};
use std::fs;
use std::os::unix::fs::PermissionsExt;
use std::net::SocketAddr;
use std::path::{Path, PathBuf};

use url::Url;
use mime_guess::MimeGuess;

use crate::config::CONFIG;
use crate::conn::Connection;
use crate::status::Status;
use crate::logger::{logger, log_err};
use crate::cgi::cgi;

pub fn handle_connection(mut conn: Connection) -> Result<(), Error> {
    let remote_addr = conn.stream.get_ref().peer_addr()?;
   
    let mut buffer: [u8; 1024] = [0; 1024];

    match conn.stream.read(&mut buffer) {
        Ok(v) => v,
        Err(_) => {
            logger(remote_addr, Status::BadRequest, "");
            conn.send_status(Status::BadRequest, None)?;
            if let Err(_) = conn.stream.shutdown() {
                log_err("SSL shutdown error");
            }
            return Ok(());
        },
    };
    let buffer = match std::str::from_utf8(&buffer) {
        Ok(v) => v,
        Err(_) => {
            logger(remote_addr, Status::BadRequest, "");
            conn.send_status(Status::BadRequest, None)?;
            if let Err(_) = conn.stream.shutdown() {
                log_err("SSL shutdown error");
            }
            return Ok(());
        }
    };

    // Stripping \r \n \0 from buffer
    let buffer = buffer.replace(&['\r', '\n', '\0'][..], "");

    if buffer.contains("..") {
        logger(remote_addr, Status::BadRequest, &buffer);
        log_err("PATH TRAVERSAL ATTEMPT");
        conn.send_status(Status::BadRequest, None)?;
        if let Err(_) = conn.stream.shutdown() {
            log_err("SSL shutdown error");
        }
        return Ok(());
    }

    let url = match Url::parse(&buffer) {
        Ok(v) => v,
        Err(_) => {
            logger(remote_addr, Status::BadRequest, &buffer);
            conn.send_status(Status::BadRequest, None)?;
            if let Err(_) = conn.stream.shutdown() {
                log_err("SSL shutdown error");
            }
            return Ok(());
        }
    };

    let hoststr = match url.host_str() {
        Some(h) => h,
        None => {
            logger(remote_addr, Status::BadRequest, &buffer);
            conn.send_status(Status::BadRequest, None)?;
            if let Err(_) = conn.stream.shutdown() {
                log_err("SSL shutdown error");
            }
            return Ok(());
        },
    };

    let vhost = match CONFIG.get_vhost(hoststr) {
        Some(host) => {
            if !url.scheme().eq("gemini") {
                logger(remote_addr, Status::ProxyRequestRefused, &buffer);
                conn.send_status(Status::ProxyRequestRefused, None)?;
                if let Err(_) = conn.stream.shutdown() {
                    log_err("SSL shutdown error");
                }
                return  Ok(());
            }
            host
        },
        None => {
            logger(remote_addr, Status::ProxyRequestRefused, &buffer);
            conn.send_status(Status::ProxyRequestRefused, None)?;
            if let Err(_) = conn.stream.shutdown() {
                log_err("SSL shutdown error");
            }
            return Ok(());
        },
    };


    let rootpath = Path::new(&vhost.rootpath).canonicalize().unwrap();
    let cgipath = rootpath.join(&vhost.cgipath);
    let mut path = rootpath.join(url.path().strip_prefix('/').unwrap_or(""));

    let mut check_path = path.clone();
    if check_path.starts_with(&cgipath) {
        while !check_path.exists() {
            check_path.pop();
        }
    }
    match check_path.canonicalize() {
        Ok(p) => {
            if !p.starts_with(rootpath) {
                logger(remote_addr, Status::NotFound, &buffer);
                conn.send_status(Status::NotFound, None)?;
                if let Err(_) = conn.stream.shutdown() {
                    log_err("SSL shutdown error");
                }
                return Ok(());
            }
        },
        Err(_) => {
            logger(remote_addr, Status::NotFound, &buffer);
            conn.send_status(Status::NotFound, None)?;
            if let Err(_) = conn.stream.shutdown() {
                log_err("SSL shutdown error");
            }
            return Ok(());
        },
    }

    // CGI
    if path.starts_with(&cgipath) {
        if handle_cgi(&mut conn, remote_addr, &url, &cgipath, &path)? {
            if let Err(_) = conn.stream.shutdown() {
                log_err("SSL shutdown error");
            }
            return Ok(());
        } else {
            logger(remote_addr, Status::CGIError, url.as_str());
            conn.send_status(Status::CGIError, None)?;
            if let Err(_) = conn.stream.shutdown() {
                log_err("SSL shutdown error");
            }
            return Ok(());
        }
    }

    if path.is_dir() {
        path.push("index.gmi");
    }

    if !path.is_file() {
        logger(remote_addr, Status::NotFound, &buffer);
        conn.send_status(Status::NotFound, None)?;
        if let Err(_) = conn.stream.shutdown() {
            log_err("SSL shutdown error");
        }
        return Ok(());
    }

    let extension = match path.extension() {
        Some(s) => s.to_str().unwrap(),
        None => "",
    };
    let mime_type = MimeGuess::from_ext(extension)
        .first_or_octet_stream().essence_str().to_owned();
    
    let mut contents: Vec<u8> = Vec::new();
    let mut file = match fs::File::open(&path) {
        Ok(f) => f,
        Err(e) => {
            log_err(&e.to_string());
            logger(remote_addr, Status::PermanentFailure, &buffer);
            conn.send_status(Status::PermanentFailure, None)?;
            if let Err(_) = conn.stream.shutdown() {
                log_err("SSL shutdown error");
            }
            return Ok(());
        },
    };
    match file.read_to_end(&mut contents) {
        Ok(c) => c,
        Err(e) => {
            log_err(&e.to_string());
            logger(remote_addr, Status::PermanentFailure, &buffer);
            conn.send_status(Status::PermanentFailure, None)?;
            if let Err(_) = conn.stream.shutdown() {
                log_err("SSL shutdown error");
            }
            return Ok(());
        },
    };

    let meta = format!("{}; lang=en", mime_type);

    logger(remote_addr, Status::Success, &buffer);
    conn.send_body(Status::Success, Some(&meta), Some(&contents))?;

    if let Err(_) = conn.stream.shutdown() {
        log_err("SSL shutdown error");
    }

    Ok(())
}

fn handle_cgi(conn: &mut Connection,
              peer_addr: SocketAddr,
              url: &Url,
              cgi_path: &PathBuf,
              full_path: &PathBuf) -> Result<bool, Error> {
    let mut path = full_path.clone();
    let mut segments = url.path_segments().unwrap();
    let mut path_info = "".to_string();

    // Find an ancestor url that matches a file
    while !path.exists() {
        if let Some(segment) = segments.next_back() {
            path.pop();
            path_info = format!("/{}{}", &segment, path_info);
        } else {
            return Ok(false);
        }
    }
    let script_name = format!("/{}", segments.collect::<Vec<_>>().join("/"));

    if path.is_file() {
        let meta = fs::metadata(&path).unwrap();
        let perm = meta.permissions();

        if path.starts_with(cgi_path) {
            if perm.mode() & 0o0111 == 0o0111 {
                cgi(conn, peer_addr, path, url, script_name, path_info)?;
                return Ok(true);
            } else {
                logger(peer_addr, Status::CGIError, url.as_str());
                conn.send_status(Status::CGIError, None)?;
                return Ok(true);
            }
        }
    }

    Ok(false)
}
