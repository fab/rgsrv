use std::io::{Write, Error};
use std::net::TcpStream;
use openssl::ssl::SslStream;

use crate::status::Status;

pub struct Connection {
    pub stream: SslStream<TcpStream>,
}

impl Connection {
    pub fn send_status(&mut self, status: Status, meta: Option<&str>) -> Result<(), Error> {
        self.send_body(status, meta, None)?;
        Ok(())
    }

    pub fn send_body(&mut self, status: Status, meta: Option<&str>, body: Option<&[u8]>)
    -> Result<(), Error> {
        let meta = match meta {
            Some(m) => m,
            None => status.to_str(),
        };

        self.send_raw(format!("{} {}\r\n", status as u8, meta).as_bytes())?;

        if let Some(b) = body {
            self.send_raw(b)?;
        }

        Ok(())
    }

    pub fn send_raw(&mut self, body: &[u8]) -> Result<(), Error> {
        self.stream.write_all(body)?;
        self.stream.flush()?;
        Ok(())
    }
}
