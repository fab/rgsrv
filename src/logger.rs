use std::net::SocketAddr;
use chrono::{Local, Datelike, Timelike};

use crate::status;

pub fn logger(addr: SocketAddr, status: status::Status, req: &str) {
    let now = Local::now();
    let now = format!("{}-{:02}-{:02}T{:02}:{:02}:{:02}",
                      now.year(), now.month(), now.day(),
                      now.hour(), now.minute(), now.second());

    println!("{} {} - {} {} - req={}", now, addr, status as u8, status.to_str(), req);
}

pub fn log_err(err: &str) {
    let now = Local::now();
    let now = format!("{}-{:02}-{:02}T{:02}:{:02}:{:02}",
                      now.year(), now.month(), now.day(),
                      now.hour(), now.minute(), now.second());

    println!("{} - ERROR={}", now, err);
}
