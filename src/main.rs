use std::net::TcpListener;
use std::sync::Arc;
use std::panic;
use std::process;

use openssl::ssl::{SslMethod, SslAcceptor, SslFiletype, SslVerifyMode};

mod threadpool;
mod config;
mod conn;
mod status;
mod request;
mod logger;
mod cgi;

use crate::config::CONFIG;

#[cfg(test)]
mod tests;

fn main() {
    // Make whole program panic when thread panics
    let orig_hook = panic::take_hook();
    panic::set_hook(Box::new(move |panic_info| {
        //invoke default handler end exit the process
        orig_hook(panic_info);
        process::exit(1);
    }));

    // Create Listener and initial Acceptor (SSL)
    let listener = TcpListener::bind(&CONFIG.general.bind).unwrap();
    let mut acceptor = SslAcceptor::mozilla_intermediate(SslMethod::tls()).unwrap();
    acceptor.set_private_key_file(&CONFIG.general.keyfile, SslFiletype::PEM).unwrap();
    acceptor.set_certificate_chain_file(&CONFIG.general.certfile).unwrap();
    acceptor.check_private_key().unwrap();
    acceptor.set_verify_callback(SslVerifyMode::PEER, |_, _| true);

    let acceptor = Arc::new(acceptor.build());

    // Create Thread Pool
    let pool = threadpool::ThreadPool::new(CONFIG.general.workers);

    // Listen for incoming tcp connections in a loop
    for stream in listener.incoming() {
        let stream = match stream {
            Ok(s) => s,
            Err(e) => {
                logger::log_err(&e.to_string());
                continue;
            },
        };

        // Clone acceptor to move into Closure
        let acceptor = acceptor.clone();

        // move job with stream and acceptor into pool for execution
        pool.execute(move || {
            match acceptor.accept(stream) {
                Ok(s) => match request::handle_connection(conn::Connection { stream: s }) {
                    Ok(_) => (),
                    Err(e) => {
                        logger::log_err(&e.to_string());
                        ()
                    },
                },
                Err(e) => {
                    logger::log_err(&e.to_string());
                    ()
                },
            }
        });
    }
    println!("Shutdown...");
}

