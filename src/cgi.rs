use std::collections::HashMap;
use std::net::SocketAddr;
use std::io;
use std::path::PathBuf;

use std::process::Command;
use std::process::Stdio;
use std::time::Duration;

use url::Url;
use process_control::{ChildExt, Control};
use openssl::hash::MessageDigest;

use crate::conn::Connection;
use crate::status::Status;
use crate::logger::logger;

fn envs(peer_addr: SocketAddr,
        conn: &Connection,
        url: &Url) -> HashMap<String, String> {
    let mut envs = HashMap::new();
    envs.insert("GATEWAY_INTERFACE".to_string(), "CGI/1.1".to_string());
    envs.insert("GEMINI_URL".to_string(), url.to_string());
    envs.insert("SERVER_NAME".to_string(), url.host_str().unwrap().to_string());
    envs.insert("SERVER_PROTOCOL".to_string(), "GEMINI".to_string());
    
    let addr = peer_addr.ip().to_string();
    envs.insert("REMOTE_ADDR".to_string(), addr.clone());
    envs.insert("REMOTE_HOST".to_string(), addr);
    envs.insert("REMOTE_PORT".to_string(), peer_addr.port().to_string());

    envs.insert("SERVER_SOFTWARE".to_string(), env!("CARGO_PKG_NAME").to_string());

    if let Some(query) = url.query() {
        envs.insert("QUERY_STRING".to_string(), query.to_string());
    }

    if let Some(cert) = conn.stream.ssl().peer_certificate() {
        if let Some(subject_name) = cert.subject_name().entries().next() {
            if let Ok(subject_name) = subject_name.data().as_utf8() {
                envs.insert("REMOTE_USER".to_string(), subject_name.to_string());
            }
        }
        if let Ok(digest) = cert.digest(MessageDigest::sha256()) {
            let mut hex = String::from("SHA256:");
            for b in digest.to_vec() {
                hex.push_str(&format!("{:02X}", b));
            }
            envs.insert("TLS_CLIENT_HASH".to_string(), hex);
        }
        envs.insert("AUTH_TYPE".to_string(), "Certificate".to_string());
    }
    
    envs
}

fn check(b: u8, peer_addr: SocketAddr, url: &Url) -> bool {
    match b {
        49 => {
            logger(peer_addr, Status::Input, url.as_str());
        },
        50 => {
            logger(peer_addr, Status::Success, url.as_str());
        },
        51..=54 => {},
        _ => {
            logger(peer_addr, Status::CGIError, url.as_str());
            return false;
        },
    }
    true
}

pub fn cgi(conn: &mut Connection,
           peer_addr: SocketAddr,
           path: PathBuf,
           url: &Url,
           script_name: String,
           path_info: String) -> Result<(), io::Error> {
    let mut envs = envs(peer_addr, conn, url);
    envs.insert("SCRIPT_NAME".to_string(), script_name);
    envs.insert("PATH_INFO".to_string(), path_info);

    let cmd = Command::new(path.to_str().unwrap())
        .env_clear()
        .envs(&envs)
        .stdout(Stdio::piped())
        .spawn()?
        .controlled_with_output()
        .time_limit(Duration::from_secs(5))
        .terminate_for_timeout()
        .wait()?;

    let cmd = match cmd {
        Some(output) => output,
        None => {
            logger(peer_addr, Status::CGIError, url.as_str());
            conn.send_status(Status::CGIError, None)?;
            return Ok(());
        },
    };

    if !cmd.status.success() {
        logger(peer_addr, Status::CGIError, url.as_str());
        conn.send_status(Status::CGIError, None)?;
        return Ok(());
    }

    let cmd = cmd.stdout;
    if cmd.len() == 0 {
        logger(peer_addr, Status::CGIError, url.as_str());
        conn.send_status(Status::CGIError, None)?;
        return Ok(());
    }
    if !check(cmd[0], peer_addr, url) {
        conn.send_status(Status::CGIError, None)?;
        return Ok(());
    }

    conn.send_raw(&cmd)?;

    Ok(())
}
