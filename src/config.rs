use std::fs;
use std::io::{Read, Error};
use std::env;

use serde::Deserialize;
use toml;

use once_cell::sync::Lazy;

#[derive(Debug, Deserialize, Clone)]
pub struct Config {
    pub general: General,
    pub vhost: Vec<VHost>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct General {
    pub bind: String,
    pub workers: usize,
    pub certfile: String,
    pub keyfile: String,
}

#[derive(Debug, Deserialize, Clone)]
pub struct VHost {
    pub servername: String,
    pub rootpath: String,
    pub cgipath: String,
}

impl Config {
    pub fn from_file(path: &str) -> Result<Config, Error> {
        let mut file = String::new();
        fs::File::open(path)?.read_to_string(&mut file)?;

        let config: Config = toml::from_str(&file)?;

        Ok(config)
    }

    pub fn load() -> Result<Config, Error> {
        let args: Vec<String> = env::args().collect();
        if args.len() != 2 {
            panic!("Please give path to config file as first and only argument");
        }
        
        let config = Config::from_file(&args[1])?;
        
        Ok(config)
    }

    pub fn get_vhost(&self, hostname: &str) -> Option<&VHost> {
        for host in &self.vhost {
            if &host.servername == hostname {
                return Some(host);
            }
        }
        None
    }
}

pub static CONFIG: Lazy<Config> = Lazy::new(|| match Config::load() {
    Ok(c) => c,
    Err(e) => {
        eprintln!("Unable to load config: {e}");
        std::process::exit(1);
    },
});

